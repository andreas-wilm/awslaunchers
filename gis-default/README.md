# Example

`NAME=wilma-test SSHKEY=wilma INSTANCE=t2.small PROFILE=wilma-gis-dev bash launch.sh`

- NAME is just for tagging
- SSHKEY is the (AWS) ssh key used for login
- PROFILE is only needed if the default AWS profile doesn't exist 
- SECGROUP: optional
- ROLE: optional
- USER: optional for tagging



