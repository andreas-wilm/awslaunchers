#!/bin/bash

if [ -z "$NAME" ]; then
    echo "FATAL: please define NAME (for tagging)" 1>&2
    exit 1
fi
if [ -z "$SSHKEY" ]; then
    echo "FATAL: please define SSHKEY (for log-in)" 1>&2
    exit 1
fi
if [ -z "$INSTANCE" ]; then
    echo "FATAL: please define INSTANCE (e.g. t2.small)" 1>&2
    exit 1
fi

if [ -z "$PROFILE" ]; then
    profilearg="--profile default"
    echo "Using default profile (overwrite by setting PROFILE)"
else
    profilearg="--profile $PROFILE"
fi
if [ ! -z "$ROLE" ]; then
    rolearg="--iam-instance-profile Name=$ROLE"
fi

# subnet-1760e573 ap-southeast-1a
# subnet-861ca8f0 ap-southeast-1b
# subnet-6c9f8d2a ap-southeast-1c
if [ ! -z "$SUBNET" ]; then
    subnetarg="--subnet $SUBNET"
fi

if [ ! -z "$SECGROUP" ]; then
    secgrouparg="--security-group-ids $SECGROUP"
fi

test -z $USER && USER=$(whoami);# for tagging only
test -z $VOLSIZE && VOLSIZE=20;# ebs volume size in GB

AMI=${AMI:-ami-0fca02518e0faeb84};# Amazon Linux 2 AMI (HVM), SSD Volume Type
ROOTDEV=${ROOTDEV:-/dev/xvda};# NOTE: root device depends on AMI


#cat<<EOF
aws ec2 run-instances --profile $PROFILE --image-id $AMI --key $SSHKEY \
    --instance-type $INSTANCE --user-data file://cloudinit.txt $rolearg $secgrouparg $subnetarg \
    --block-device-mapping "DeviceName=$ROOTDEV,Ebs={VolumeSize=$VOLSIZE}" \
    --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$NAME},{Key=user,Value=$USER}]"
#EOF
echo "INFO: for listing use: aws --profile $PROFILE ec2 describe-instances --filters 'Name=tag:user,Values=$USER'"
