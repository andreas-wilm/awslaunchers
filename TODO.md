on d instances mount unused nvme as /data

# recipes

## nim

```
#!/bin/bash

VER=nim-0.19.0

cd /tmp/
wget -nd https://nim-lang.org/download/${VER}.tar.xz
tar xf ${VER}.tar.xz
cd ${VER}/


sh build.sh
bin/nim c koch
./koch tools

rm -rf c_code/
cd ..
mv ${VER} /usr/local
# FIXME add to path
# might require chmod 755 in bin
rm  ${VER}.tar.xz
```


## Bioconda
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
as ec2-user
```
